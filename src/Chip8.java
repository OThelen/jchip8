import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

public class Chip8 {
	//status
	public boolean running;
	//current cpu opcode
	private short opcode;
	//the memory of the Chip8 system 4096*8bit
	private byte[] memory;
	//the program
	private byte[] program;
	//16 Vx registers with 8bit each
	private byte[] V;
	//the index register I
	private short I;
	//the program counter
	private short pc;
	//graphics memory
	private byte[][] screen;
	//delay timer
	private byte DT;
	//sound timer
	private byte ST;
	//stack for jump adresses
	private short[] stack;
	//stackpointer
	private short sp;
	//keys
	public boolean[] key;	
	//update screen
	private boolean updateScreen;
	
	//thanks to Laurence Muller for the font
	private int[] font =
		{ 
		  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		  0x20, 0x60, 0x20, 0x20, 0x70, // 1
		  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
		};
	
	//DEBUG
	//executed commands
	public int[] debug_exCommands;
	
	public Chip8(){
		this.initialize();
	}
	
	public void restart(){
		this.initialize();
		this.reloadProgram();
	}
	
	/**
	 * Initialize all variables
	 */
	public void initialize(){
		this.opcode = 0;
		this.V = new byte[16];
		this.I = 0;
		this.pc = 512;
		this.DT = 0;
		this.ST = 0;
		this.sp = 0;
		this.running = false;
		
		this.debug_exCommands = new int[34];
		for(int i=0;i<34;i++){
			//this.debug_exCommands[i] = 0;
		}
		
		//intialize the memory
		this.memory = new byte[4096];
		for(int i=0;i<4096;i++){
			this.memory[i] = 0;
		}
		
		for(int i = 0; i < 80; ++i)
		    memory[i] = (byte) font[i];
		
		//initialze the V registers
		this.V = new byte[16];
		for(int i=0;i<16;i++){
			this.V[i] = 0;
		}
		
		//initialize the screen
		this.screen = new byte[64][32];
		clearScreen();
		
		//initialize the stack
		this.stack = new short[16];
		for(int i=0;i<16;i++){
			this.stack[i] = 0;
		}
		
		//initialize the screen
		this.key = new boolean[16];
		for(int i=0;i<16;i++){
			this.key[i] = false;
		}
	}
	
	/**
	 * Reset the screen
	 */
	private void clearScreen(){
		for(int i=0;i<64;i++){
			for(int j=0;j<32;j++){
				this.screen[i][j] = 0;
			}
		}
	}
	
	private void reloadProgram(){
		this.initialize();
		
    	//Load all bytes into the memory
		for(int i=512;i<512+this.program.length;i++){
			this.memory[i] = this.program[i-512];
		}
		
		this.running = true;
	}

	public int loadProgram(Path filePath){
	    try {	    	
			//Read all bytes
	    	byte[] fileByteArray = Files.readAllBytes(filePath);

	    	this.program = new byte[fileByteArray.length];
	    	
	    	//Load all bytes into the memory
			for(int i=512;i<512+fileByteArray.length;i++){
				this.program[i-512] = fileByteArray[i-512];
				this.memory[i] = fileByteArray[i-512];
			}
			
	    	this.running = true;
	    	this.clearScreen();
			
			return fileByteArray.length;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    return -1;
	}
	
	public byte[][] getScreen(){
		return screen;
	}
	
	public String getOPCodeString(){
		return getHexValue16(opcode);
	}
	
	public short getOPcode(){
		return opcode;
	}
	
	public short getPC(){
		return this.pc;
	}
	
	public short getI(){
		return this.I;
	}
	
	public short getDT(){
		return this.DT;
	}
	
	public short getST(){
		return this.ST;
	}
	
	public short getSP(){
		return this.sp;
	}
	
	public byte[] getV(){
		return this.V;
	}

	/**
	 * Check if the screen was updated
	 * @return
	 */
	public boolean getUpdateScreen(){
		boolean result = updateScreen;
		this.updateScreen = false;
		return result;
	}
	
	public String getBinValue8(short value){
		return Integer.toBinaryString(value & 255 | 256).substring(1);
	}
	
	public String getHexValue8(short value){
		return Integer.toHexString(value & 255 | 256).substring(1);
	}
	
	public String getHexValue16(short value){
		return Integer.toHexString(value & 65535 | 65536).substring(1);
	}
	
	private void printHexValue16(short value){
		System.out.println(Integer.toHexString(value & 65535 | 65536).substring(1)+ " ");
	}
	
	/**
	 * Dump the current memoryarray
	 * @param onlyWorking
	 */
	public void dumpMemory(boolean onlyWorking){
		int i=0;
		if(onlyWorking)
			i=512;
		
		for(;i<4096;i++){
			if((i+1)%16==0 && i>0){
				System.out.println(Integer.toHexString(this.memory[i] & 255 | 256).substring(1) +" ");
			}else{
				System.out.print(Integer.toHexString(this.memory[i] & 255 | 256).substring(1) +" ");
			}
		}
	}

	/**
	 * Print the current registerarray
	 */
	public void dumpRegisters(){
		for(int i=0;i<15;i++){
			System.out.print(mod(this.V[i],256) +" - ");
		}
		System.out.println(mod(this.V[15],256));
	}
	
	/**
	 * Print the current stackarray
	 */
	public void dumpStack(){
		System.out.println("STACK:");
		for(int i=0;i<15;i++){
			System.out.print(this.stack[i] +" - ");
		}
		System.out.println(this.stack[15]);
	}
	
	/**
	 * Print the current state of the screenarray
	 */
	public void dumpScreen(){
		for(int y=0;y<32;y++){
			for(int x=0;x<64;x++){
				if(this.screen[x][y] == 1)
					System.out.print(1);
				else
					System.out.print(0);
			}
			
			System.out.println(" ");
		}
	}
	
	/**
	 * Modulo operation
	 * @param x
	 * @param y
	 * @return
	 */
	private int mod(int x, int y)
	{
	    int result = x % y;
	    if (result < 0)
	    {
	        result += y;
	    }
	    return result;
	}
	
	/**
	 * Simulate a cpu cycle
	 */
	public void runCycle(){
		//get the current opcode that needs to be executed
		this.opcode = (short) ( this.memory[pc] << 8 & 0xFF00 |  this.memory[pc+1] & 0xFF);

		if(this.opcode != 0){
			executeOpcode();
			
			//update the delay timer
			if(this.DT > 0){
				this.DT--;
			}
			
			//update the sound timer
			if(this.ST > 0){
				this.ST--;
			}
		}else{
			System.out.println("End of program!");
		}
	}
	
	/**
	 * Execute the current opcode
	 */
	public void executeOpcode(){
		boolean setPC = true;
		
		switch((short) ( this.opcode & 0xF000)){
			case (short) 0x0000:
				switch((short) ( this.opcode & 0x000F)){
					case (short) 0x0000:
						clearScreen();
					
						//this.debug_exCommands[0]++;
					break;
					case (short) 0x000E:
						this.sp--;
						this.pc = stack[sp];
						this.stack[sp] = 0;
						
						//this.debug_exCommands[1]++;
					break;
					default:System.out.println("Unknown opcode! 0XXX");
				}
			break;
			case (short) 0x1000:
				//set program counter to the 12bit adress given
				this.pc = (short) (this.opcode & 0x0FFF);
				setPC = false;
				
				//this.debug_exCommands[2]++;
			break;
			case (short) 0x2000:
				//save current program counter value on the stack
				stack[sp] = (short) (this.pc);
				//increase stack pointer
				sp++;
				
				//set program counter to the 12bit adress given
				this.pc = (short) (this.opcode & 0x0FFF);
				setPC = false;
				
				//this.debug_exCommands[3]++;
			break;
			case (short) 0x3000:
				//If Vx is equal to the byte provided: skip the next instruction
				if(this.V[(((short) (this.opcode & 0x0F00)) >> 8)] == (byte) (this.opcode & 0x00FF)){
					this.pc += 4;
					setPC = false;
				}
			
				//this.debug_exCommands[4]++;
			break;
			case (short) 0x4000:
				//If Vx is not equal to the byte provided: skip the next instruction
				if(this.V[(((short) (this.opcode & 0x0F00)) >> 8)] != (byte) (this.opcode & 0x00FF)){
					this.pc += 4;
					setPC = false;
				}
			
				//this.debug_exCommands[5]++;
			break;
			case (short) 0x5000:
				//If Vx is equal to Vy: skip the next instruction
				if(this.V[(((short) (this.opcode & 0x0F00)) >> 8)] == this.V[(((short) (this.opcode & 0x00F0)) >> 4)]){
					this.pc += 4;
					setPC = false;
				}
			
				//this.debug_exCommands[6]++;
			break;
			case (short) 0x6000:
				//set Vx to NN
				this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = (byte) (this.opcode & 0x00FF);
			
				//this.debug_exCommands[7]++;
			break;
			case (short) 0x7000:
				//set Vx to Vx + NN
				this.V[(((short) (this.opcode & 0x0F00)) >> 8)] += (byte) (this.opcode & 0x00FF);
			
				//this.debug_exCommands[8]++;
			break;
			case (short) 0x8000:
				switch((short) ( this.opcode & 0x000F)){
					case (short) 0x0000:
						//set Vx to value of Vy
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = this.V[(((short) (this.opcode & 0x00F0)) >> 4)];
						//this.debug_exCommands[9]++;
					break;
					case (short) 0x0001:
						//set Vx to (Vx or Vy)
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] |= this.V[(((short) (this.opcode & 0x00F0)) >> 4)];
						//this.debug_exCommands[10]++;
					break;
					case (short) 0x0002:
						//set Vx to (Vx and Vy)
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] &= this.V[(((short) (this.opcode & 0x00F0)) >> 4)];
						//this.debug_exCommands[11]++;
					break;
					case (short) 0x0003:
						//set Vx to (Vx xor Vy)
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] ^= this.V[(((short) (this.opcode & 0x00F0)) >> 4)];
						//this.debug_exCommands[12]++;
					break;
					case (short) 0x0004:
						int vxplusvy = this.V[(((short) (this.opcode & 0x0F00)) >> 8)] +  this.V[(((short) (this.opcode & 0x00F0)) >> 4)];
						
						if(vxplusvy > 255){
							this.V[0xF] = 1;
						}else{
							this.V[0xF] = 0;
						}
						
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = (byte) (vxplusvy);
						
						//this.debug_exCommands[13]++;
					break;
					case (short) 0x0005:
						if(mod(this.V[(((short) (this.opcode & 0x0F00)) >> 8)],256) > mod(this.V[(((short) (this.opcode & 0x00F0)) >> 4)],256)){
							this.V[0xF] = 1;
						}else{
							this.V[0xF] = 0;
						}
								
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] -= this.V[(((short) (this.opcode & 0x00F0)) >> 4)];
						//this.debug_exCommands[14]++;
					break;
					case (short) 0x0006:
						if(mod(this.V[(((short) (this.opcode & 0x0F00)) >> 8)],256) % 2 == 1){
							this.V[0xF] = 1;
						}else{
							this.V[0xF] = 0;
						}
					
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = (byte) (this.V[(((short) (this.opcode & 0x0F00)) >> 8)] >> 1);
						//this.debug_exCommands[15]++;
					break;
					case (short) 0x0007:
						if(mod((short) this.V[(((short) (this.opcode & 0x00F0)) >> 4)],256) > mod((short) this.V[(((short) (this.opcode & 0x0F00)) >> 8)],256)){
							this.V[0xF] = 1;
						}else{
							this.V[0xF] = 0;
						}
								
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = (byte) ((short) this.V[(((short) (this.opcode & 0x00F0)) >> 4) - (short) this.V[(((short) (this.opcode & 0x0F00)) >> 8)]]);
						//this.debug_exCommands[16]++;
					break;
					case (short) 0x000E:
						
						if((this.V[(((short) (this.opcode & 0x0F00)) >> 8)] & 256) != 0){
							this.V[0xF] = 1;
						}else{
							this.V[0xF] = 0;
						}
					
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = (byte) (this.V[(((short) (this.opcode & 0x0F00)) >> 8)] << 1);
						//this.debug_exCommands[17]++;
					break;
					default:System.out.println("Unknown opcode! 8XXX");
				}
			break;
			case (short) 0x9000:
				//If Vx is not equal to Vy: skip the next instruction
				if(this.V[(((short) (this.opcode & 0x0F00)) >> 8)] != this.V[(((short) (this.opcode & 0x00F0)) >> 4)]){
					this.pc += 4;
					setPC = false;
				}
				//this.debug_exCommands[18]++;
			break;
			case (short) 0xA000:
				//set I to the 12bit value given
				this.I = (short) (this.opcode & 0x0FFF);
				//this.debug_exCommands[19]++;
			break;
			case (short) 0xB000:
				this.pc = (short) ((short) (this.opcode & 0x0FFF)+ this.V[0]);
				setPC = false;
				//this.debug_exCommands[20]++;
			break;
			case (short) 0xC000:
				Random rn = new Random();
				int randomNumber = rn.nextInt(256);
				
				this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = (byte) (randomNumber & (byte) (this.opcode & 0x00FF));
				//this.debug_exCommands[21]++;
			break;
			case (short) 0xD000:
				boolean collision = false;
			
				//Display
				for(int y=0;y<(this.opcode & 0x000F);y++){
					for(int x=0;x<8;x++){
						byte oldPixel = this.screen[mod(this.V[(((short) (this.opcode & 0x0F00)) >> 8)]+x,64)][mod(this.V[(((short) (this.opcode & 0x00F0)) >> 4)]+y,32)];
						this.screen[mod(this.V[(((short) (this.opcode & 0x0F00)) >> 8)]+x,64)][mod(this.V[(((short) (this.opcode & 0x00F0)) >> 4)]+y,32)] ^= ((this.memory[this.I+y] >> 7-x) & 1);
						if(oldPixel == 1 && this.screen[mod(this.V[(((short) (this.opcode & 0x0F00)) >> 8)]+x,64)][mod(this.V[(((short) (this.opcode & 0x00F0)) >> 4)]+y,32)] == 0){
							collision = true;
						}
					}
				}
				
				updateScreen = true;
				
				if(collision)
					this.V[0xF] = 1;
				else
					this.V[0xF] = 0;
				
				//this.debug_exCommands[22]++;
			break;
			case (short) 0xE000:
				switch((short) ( this.opcode & 0x000F)){
					case (short) 0x000E:
						if(this.key[this.V[(((short) (this.opcode & 0x0F00)) >> 8)]]){
							this.pc += 4;
							setPC = false;
						}
						//this.debug_exCommands[23]++;
					break;
					case (short) 0x0001:
						if(!this.key[this.V[(((short) (this.opcode & 0x0F00)) >> 8)]]){
							this.pc += 4;
							setPC = false;
						}
						//this.debug_exCommands[24]++;
					break;
					default:System.out.println("Unknown opcode! EXXX");
				}
			break;
			case (short) 0xF000:
				switch((short) ( this.opcode & 0x00FF)){
					case (short) 0x0007:
						this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = this.DT;
						//this.debug_exCommands[25]++;
					break;
					case (short) 0x000A:
						byte keypress = -1;

						for(int i=0;i<16;i++){
							if(this.key[i]){
								keypress = (byte) i;
							}
						}

					    setPC = false;
					    
					    if(keypress == -1){
					    	//do nothing
					    }else{
					    	this.V[(((short) (this.opcode & 0x0F00)) >> 8)] = keypress;
					    	this.pc += 2;
					    }

						//this.debug_exCommands[26]++;
					break;
					case (short) 0x0015:
						this.DT = this.V[(((short) (this.opcode & 0x0F00)) >> 8)];
						//this.debug_exCommands[27]++;
					break;
					case (short) 0x0018:
						this.ST = this.V[(((short) (this.opcode & 0x0F00)) >> 8)];
						//this.debug_exCommands[28]++;
					break;
					case (short) 0x001E:
						this.I += (this.V[(((short) (this.opcode & 0x0F00)) >> 8)] & 0xFF);
						//this.debug_exCommands[29]++;
					break;
					case (short) 0x0029:
						this.I = (short) (5 * (this.V[(((short) (this.opcode & 0x0F00)) >> 8)] & 0xF));

						//this.debug_exCommands[30]++;
					break;
					case (short) 0x0033:
						short number = (short) mod((this.V[(((short) (this.opcode & 0x0F00)) >> 8)] & 0xFF),256);
						
						
						this.memory[this.I+2] = (byte) (number%10);
						number = (byte) (number/10);
						this.memory[this.I+1] = (byte) (number%10);
						number = (byte) (number/10);
						this.memory[this.I] = (byte) (number%10);
						
						//this.debug_exCommands[31]++;
					break;
					case (short) 0x0055:
						for(int i=0;i<=(((short) ((this.opcode & 0x0F00)) >> 8) & 0xFF);i++){
							this.memory[this.I + i] = this.V[i];
						}
						//this.debug_exCommands[32]++;
					break;
					case (short) 0x0065:
						for(int i=0;i<=(((short) ((this.opcode & 0x0F00)) >> 8) & 0xFF);i++){
							this.V[i] = this.memory[this.I + i];
						}
						//this.debug_exCommands[33]++;
					break;
					default:System.out.println("Unknown opcode! FXXX");printHexValue16(this.opcode);
				}
			break;
			default:System.out.println("Unknown opcode!");
		}
		
		if(setPC){
			//increase the program counter
			this.pc += 2;
		}
	}
}
