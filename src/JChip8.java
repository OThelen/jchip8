import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GridLayout;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

class Surface extends JPanel implements ActionListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int DELAY = 3;
    private Timer timer;
    private Chip8 myChip8;
    private JChip8 jChip8;
    private int counter = 61;
    private short lastopcode;

    public Surface(Chip8 pChip8,JChip8 pJChip8) {
        this.myChip8 = pChip8;
        this.jChip8 = pJChip8;
    	initTimer();
    }

    private void initTimer() {
        timer = new Timer(DELAY, this);
        timer.start();
    }
    
    public Timer getTimer() {
        return timer;
    }
    
    private void doDrawing(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        g2d.setPaint(new Color(0, 0, 0));
        g2d.fillRect(0, 0, 640, 320);
        
        byte[][] chip8Screen = myChip8.getScreen();

        for(int y=0;y<32;y++){
        	for(int x=0;x<64;x++){
        		if(chip8Screen[x][y] == 1){
        			g2d.setPaint(new Color(255, 255, 255));
        			g2d.fillRect(x*10, y*10, 10, 10);
        		}else{
        			g2d.setPaint(new Color(0, 0, 0));
        			g2d.fillRect(x*10, y*10, 10, 10);
        		}
        	}
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        doDrawing(g);
    }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(jChip8.debug && (this.counter > 60 | (jChip8.singleStepMode && lastopcode != myChip8.getOPcode()))){
			for(int i=0;i<16;i++){
				jChip8.textFields[i].setText("V"+ Integer.toHexString(i).toUpperCase() +":"+ myChip8.getHexValue8(myChip8.getV()[i]));
			}
			
			jChip8.textFields[16].setText("OPC:"+ String.valueOf(myChip8.getOPCodeString()));
			jChip8.textFields[17].setText("PC:"+ myChip8.getHexValue16(myChip8.getPC()));
			jChip8.textFields[18].setText("I:"+  myChip8.getHexValue16(myChip8.getI()));
			jChip8.textFields[19].setText("DT:"+ myChip8.getHexValue8(myChip8.getDT()));
			jChip8.textFields[20].setText("ST:"+ myChip8.getHexValue8(myChip8.getST()));
			jChip8.textFields[21].setText("SP:"+ myChip8.getSP());
			
			this.counter = -1;
		}
		
		this.counter++;
		
		lastopcode = myChip8.getOPcode();
		
		if(myChip8.getUpdateScreen())
			repaint();
	}
}

public class JChip8 extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public boolean fullscreen = false;
	public boolean debug = false;
	public boolean singleStepMode = false;
	public Chip8 myChip8;
	
	public JButton buttonExit;
	public JButton buttonStep;
	public JButton buttonRun;
	public JButton buttonRestart;
	
	public JPanel upperPanel;
	public JPanel lowerPanel;
	
	public JTextField[] textFields;
	
    public JChip8(Chip8 pChip8) {
    	this.textFields = new JTextField[24];
    	
    	if(fullscreen == true){
            setUndecorated(true);
    	}

        initUI(pChip8);
    }
    
    private class Chip8Thread implements Runnable {
    	private Chip8 myChip8;
    	private JChip8 myJChip8;
    	
    	public Chip8Thread(Chip8 pChip8,JChip8 pJChip8){
    		myChip8 = pChip8;
    		myJChip8 = pJChip8;
    	}

		@Override
		public void run() {
		    long start = 0, diff, wait;
		    wait = 3;
		    
		    //slow down the simulation
		    while(true){
		    	if(myChip8.running && !myJChip8.singleStepMode)
		    		myChip8.runCycle();
		    	
			    diff = System.currentTimeMillis() - start;
			    if (diff < wait) {
			        try {
						Thread.sleep(wait - diff);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
			    start = System.currentTimeMillis();
		    }
		}
    }
 
    /**
     * Activate the program in fullscreen mode
     * @param frame
     * @param doPack
     * @return
     */
    static public boolean fullScreen(final JFrame frame, boolean doPack) {

        GraphicsDevice device = frame.getGraphicsConfiguration().getDevice();
        boolean result = device.isFullScreenSupported();

        if (result) {
            frame.setUndecorated(true);
            frame.setResizable(true);

            frame.addFocusListener(new FocusListener() {

                @Override
                public void focusGained(FocusEvent arg0) {
                    frame.setAlwaysOnTop(true);
                }

                @Override
                public void focusLost(FocusEvent arg0) {
                    frame.setAlwaysOnTop(false);
                }
            });

            if (doPack)
                frame.pack();

            device.setFullScreenWindow(frame);
        }
        else {
            frame.setPreferredSize(frame.getGraphicsConfiguration().getBounds().getSize());

            if (doPack)
                frame.pack();

            frame.setResizable(true);

            frame.setExtendedState(Frame.MAXIMIZED_BOTH);
            boolean successful = frame.getExtendedState() == Frame.MAXIMIZED_BOTH;

            frame.setVisible(true);

            if (!successful)
                frame.setExtendedState(Frame.MAXIMIZED_BOTH);
        }
        return result;
    }
    
    class LoadAction extends AbstractAction {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Chip8 myChip8;
    	
        public LoadAction(String text,Chip8 pChip8) {
            super(text);
            
            myChip8 = pChip8;
        }
        public void actionPerformed(ActionEvent e) {
          //Create a file chooser
          final JFileChooser fc = new JFileChooser();
            
          //In response to a button click:
          fc.showOpenDialog(JChip8.this);
          
          //load the program
          myChip8.loadProgram(fc.getSelectedFile().toPath());
          myChip8.restart();
        }
    }
    
    class DebugAction extends AbstractAction {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JChip8 myJChip8;
    	
        public DebugAction(String text,Chip8 pChip8,JChip8 pJChip8) {
            super(text);
            
            myJChip8 = pJChip8;
        }
        public void actionPerformed(ActionEvent e) {
        	if(myJChip8.debug){
        		myJChip8.debug = false;
        		myJChip8.lowerPanel.setVisible(false);
        		myJChip8.pack();
        	}else{
        		myJChip8.debug = true;
        		myJChip8.lowerPanel.setVisible(true);
        		myJChip8.pack();
        	}
        }
    }
    
    private void initUI(Chip8 pChip8) {
    	this.myChip8 = pChip8;
    	
    	//create the menubar
    	JMenuBar menuBar;
    	JMenu menu;
    	JMenuItem menuItem;
    	JCheckBoxMenuItem cbMenuItem;
    	
    	//Create the menu bar.
    	menuBar = new JMenuBar();
    	
    	//Build the first menu.
    	menu = new JMenu("Menu");
    	menu.setMnemonic(KeyEvent.VK_M);
    	menuBar.add(menu);
    	
    	//a JMenuItem
    	LoadAction loadAction = new LoadAction("Load File",myChip8);
    	menuItem = new JMenuItem(loadAction);
    	menu.add(menuItem);
    	
    	//a CheckBoxItem
    	menu.addSeparator();
    	DebugAction debugAction = new DebugAction("Debug",myChip8,this);
    	cbMenuItem = new JCheckBoxMenuItem(debugAction);
    	menu.add(cbMenuItem);
    	
    	this.setJMenuBar(menuBar);
    	
    	//make the JFrame visible
    	this.setVisible(true);
    	
    	//set the background color to black
    	this.getContentPane().setBackground(new Color(0, 0, 0));
    	
    	//create the surface for the screen of the chip8
	    final Surface surface = new Surface(pChip8,this);
	    surface.setBackground(new Color(0, 0, 0));
	    surface.setPreferredSize(new Dimension(640,320));

	    //create the upperpanel for the screen and add the surface
	    upperPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
	    upperPanel.setBackground(new Color(0, 0, 0));
	    getContentPane().add(upperPanel, "North");
	    upperPanel.add(surface);
	    
	    //add the closing action for the jframe
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                //stop the timer
            	Timer timer = surface.getTimer();
                timer.stop();
                
                //if the debug mode is activated
                if(debug){
                    String[] opcodeNames = {"00E0","00EE","1nnn","2nnn","3xkk","4xkk","5xy0","6xkk","7xkk","8xy0","8xy1","8xy2","8xy3","8xy4","8xy5","8xy6","8xy7","8xyE","9xy0","Annn","Bnnn","Cxkk","Dxyn","Ex9E","ExA1","Fx07","Fx0A","Fx15","Fx18","Fx1E","Fx29","Fx33","Fx55","Fx65"};
                    
                    myChip8.dumpMemory(false);
                    for(int i=0;i<myChip8.debug_exCommands.length;i++){
                    	if(myChip8.debug_exCommands[i] != 0){
                    		System.out.println(i + " - " + opcodeNames[i] + " - "+ myChip8.debug_exCommands[i]);
                    	}
                    }
                    
                    myChip8.dumpStack();
                }
            }
        });
        
        //create the thread for the chip8
        Chip8Thread myChip8Thread = new Chip8Thread(myChip8, this);
        Thread tChip8Thread = new Thread(myChip8Thread);
        tChip8Thread.start();

        //add the lower panel if the debug mode is activated
    	lowerPanel = new JPanel(new GridLayout(0,8,2,2));

        lowerPanel.setPreferredSize(new Dimension(640,100));
        
        getContentPane().add(lowerPanel, "South");

        for(int i=0;i<24;i++){
	        this.textFields[i] = new JTextField(3); 
	        lowerPanel.add(this.textFields[i]);	
        }

        this.buttonExit = new JButton("Exit");
        this.buttonExit.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 
        		  System.exit(0);
        		  } 
        		} );
        
        this.buttonStep = new JButton("Step");
        this.buttonStep.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 
        		  	singleStepMode = true;
        		  	myChip8.runCycle();
        		  } 
        		} );
        
        this.buttonRun = new JButton("Run");
        this.buttonRun.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 
        		  	singleStepMode = false;
        		  } 
        		} );
        
        this.buttonRestart = new JButton("Restart");
        this.buttonRestart.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 
        		  	myChip8.restart();
        		  } 
        		} );

        lowerPanel.add(this.buttonRun);
        lowerPanel.add(this.buttonStep);
        lowerPanel.add(this.buttonRestart);
        lowerPanel.add(this.buttonExit);
        
        lowerPanel.setVisible(false);
        
        //add the title
        setTitle("JChip8");
        setLocationRelativeTo(null);        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        if(this.fullscreen){
        	fullScreen(this,true);
        }else{
        	pack();
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
            	//create the chip8
            	final Chip8 myChip8 = new Chip8();
            	
                new JChip8(myChip8); 
               
                //Listen for keypresses
                KeyboardFocusManager keyManager;
                keyManager=KeyboardFocusManager.getCurrentKeyboardFocusManager();
                keyManager.addKeyEventDispatcher(new KeyEventDispatcher() {

                  @Override
                  public boolean dispatchKeyEvent(KeyEvent e) {
                    if(e.getID()==KeyEvent.KEY_PRESSED){
	                    switch(e.getKeyCode()){
	            			case KeyEvent.VK_1: myChip8.key[0] = true;break;
	            			case KeyEvent.VK_2: myChip8.key[1] = true;break;
	            			case KeyEvent.VK_3: myChip8.key[2] = true;break;
	            			case KeyEvent.VK_4: myChip8.key[3] = true;break;
	            			case KeyEvent.VK_Q: myChip8.key[4] = true;break;
	            			case KeyEvent.VK_W: myChip8.key[5] = true;break;
	            			case KeyEvent.VK_E: myChip8.key[6] = true;break;
	            			case KeyEvent.VK_R: myChip8.key[7] = true;break;
	            			case KeyEvent.VK_A: myChip8.key[8] = true;break;
	            			case KeyEvent.VK_S: myChip8.key[9] = true;break;
	            			case KeyEvent.VK_D: myChip8.key[10] = true;break;
	            			case KeyEvent.VK_F: myChip8.key[11] = true;break;
	            			case KeyEvent.VK_Z: myChip8.key[12] = true;break;
	            			case KeyEvent.VK_X: myChip8.key[13] = true;break;
	            			case KeyEvent.VK_C: myChip8.key[14] = true;break;
	            			case KeyEvent.VK_V: myChip8.key[15] = true;break;
	            			case KeyEvent.VK_ESCAPE: System.exit(0);;break;
	            		}
	                    	
                      return true;
                    }else if(e.getID()==KeyEvent.KEY_RELEASED){
                		switch(e.getKeyCode()){
	            			case KeyEvent.VK_1: myChip8.key[0] = false;break;
	            			case KeyEvent.VK_2: myChip8.key[1] = false;break;
	            			case KeyEvent.VK_3: myChip8.key[2] = false;break;
	            			case KeyEvent.VK_4: myChip8.key[3] = false;break;
	            			case KeyEvent.VK_Q: myChip8.key[4] = false;break;
	            			case KeyEvent.VK_W: myChip8.key[5] = false;break;
	            			case KeyEvent.VK_E: myChip8.key[6] = false;break;
	            			case KeyEvent.VK_R: myChip8.key[7] = false;break;
	            			case KeyEvent.VK_A: myChip8.key[8] = false;break;
	            			case KeyEvent.VK_S: myChip8.key[9] = false;break;
	            			case KeyEvent.VK_D: myChip8.key[10] = false;break;
	            			case KeyEvent.VK_F: myChip8.key[11] = false;break;
	            			case KeyEvent.VK_Z: myChip8.key[12] = false;break;
	            			case KeyEvent.VK_X: myChip8.key[13] = false;break;
	            			case KeyEvent.VK_C: myChip8.key[14] = false;break;
	            			case KeyEvent.VK_V: myChip8.key[15] = false;break;
                		}
                    }
                    return false;
                  }

                });
               
            }
        });
    }
}